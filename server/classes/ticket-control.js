const fs = require("fs");

/**
 * @param numeroTicket
 * @param escritorio es en la casilla donde se va a atender al ticket
 */
class Ticket {
  constructor(numeroTicket, escritorio){
    this.numeroTicket = numeroTicket;
    this.escritorio = escritorio;
  }
}

/**
 * @property {hoy} * es el día actual en que se crea el ticket
 * @property {ultimo} * ultimo numero de ticket almacenado en el sistema
 * @method reiniciarConteo cambia el ultimo ticket almacenado a 0 y guardar
 * @method siguiente agrega un ultimo ticket y guardar
 * @method grabarArchivo guarda los nuevos tickets
 */
class TicketControl {
  constructor() {
    this.ultimo = 0;
    this.hoy = new Date().getDate();
    this.tickets = [];
    this.ultimos4= [];

    //simulamos db
    let data = require("../data/data.json");

    //si es el mismo día seguimos operando normalmente
    if(data.hoy == this.hoy){
      this.ultimo = data.ultimo;
      this.tickets = data.tickets;
      this.ultimos4 = data.ultimos4;
    }
    //si es otro día reiniciamos la data
    else{
      this.reiniciarConteo();
    }
  }

  //===================================== Metodos de la clase =====================================
  reiniciarConteo() {
    this.ultimo = 0;
    this.tickets = [];
    this.ultimos4 = [];
    console.log('Se ha inicializado el sistema')
    this.grabarArchivo();
  }

  siguiente() {
      this.ultimo += 1;
      //generamos un nuevo ticket y lo pusheamos
      let ticket = new Ticket(this.ultimo, null);
      this.tickets.push(ticket)
      
      //guardamos el nuevo ticket
      this.grabarArchivo();
      return `Ticket ${this.ultimo}`;
  }

  getUltimoTicket () {
    return `Ticket ${this.ultimo}`;
  }

  getUltimos4 () {
    return this.ultimos4;
  }

  atenderTicket(escritorio) {
    if(!this.tickets.length > 0){
      return 'No hay tickets'
    }

    //obtenemos el numero del primer ticket pendiente
    let numeroTicket = this.tickets[0].numeroTicket;
    //y lo eliminamos del array de tickets pendientes
    this.tickets.shift();

    //generamos un nuevo ticket por atender(previamente eliminado de la lista de espera)
    let atenderTicket = new Ticket(numeroTicket, escritorio);
    //y lo agregamos a la lista de ultimos 4 ticekts
    this.ultimos4.unshift(atenderTicket);

    if(this.ultimos4 > 4){
      this.ultimos4.pop();
    }

    //guardamos...
    this.grabarArchivo();
    return atenderTicket;
  }

  grabarArchivo () {
    let jsonData = { ultimo: this.ultimo, hoy: this.hoy, tickets: this.tickets, ultimos4: this.ultimos4 };
    let jsonDataString = JSON.stringify(jsonData);

    //en este path - guardar este json
    fs.writeFileSync("./server/data/data.json", jsonDataString);
  }
}

module.exports = {
  TicketControl,
};
