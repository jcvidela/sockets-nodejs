const { io } = require('../server');
const { TicketControl } = require('../classes/ticket-control')

const ticketControl = new TicketControl();

//cuando el cliente se conecte
io.on('connection', (client) => {
    //devolvemos el siguiente ticket cuando se emita el evento 'siguienteTicket'
    client.on('siguienteTicket', (data, callback) => {
        let siguiente = ticketControl.siguiente();
        callback(siguiente)
    })

    //emitimos el ultimo ticket para ser consumido cuando se cargue la pagina
    client.emit('estadoActual', {
        actual: ticketControl.getUltimoTicket(),
        ultimos4: ticketControl.getUltimos4()
    });

    client.on('atenderTicket', (data, callback) => {
        if(!data.escritorio){
            return 'El escritorio es necesario'
        }

        let atenderTicket = ticketControl.atenderTicket(data.escritorio);
        callback(atenderTicket)

        client.broadcast.emit('ultimos4', {
            ultimos4: ticketControl.getUltimos4()
        });
    })
});