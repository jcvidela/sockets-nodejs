var socket = io();

var searchParams = new URLSearchParams(window.location.search);
var label = $('small');

if (!searchParams.has("escritorio")) {
  window.location = "index.html";
  throw new Error("El escritorio es necesario");
}

var escritorio = searchParams.get('escritorio');
$('h1').text('Escritorio' + ' ' + escritorio);

$('button').click(function() {
    socket.emit('atenderTicket', { escritorio: escritorio }, function(response) {
        if(response === "No hay tickets"){
            return alert(response)
        }
        label.text('Ticket' + ' ' + response.numeroTicket)
    })
})