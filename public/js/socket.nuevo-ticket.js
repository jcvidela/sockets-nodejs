var socket = io();
var label = $('#lblNuevoTicket');

// detectamos cuando hay una conexión del servidor
socket.on('connect', function() {
    console.log('conectado al servidor')
});

// detectamos cuando hay una desconexión en el servidor
socket.on('disconnect', function() {
    console.log('desconectado del servidor')
});

// detectamos el evento 'estadoActual' del servidor
socket.on('estadoActual', function(data) {
    let ticketActual = data.actual
    label.text(ticketActual)
})

// generamos un nuevo ticket
$('button').click(function() {
    // emitimos el evento 'siguienteTicket'
    socket.emit('siguienteTicket', null, function(siguienteTicket) {
        label.text(siguienteTicket)
    })
})